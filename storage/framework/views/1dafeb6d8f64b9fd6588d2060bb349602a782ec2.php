<!doctype html>
<html lang="<?php echo e(str_replace('_', '-', app()->getLocale())); ?>">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="<?php echo e(csrf_token()); ?>">

    <title>Condormix</title>

    <!-- Styles -->
    <link href="<?php echo e(asset('css/app.css')); ?>" rel="stylesheet">
</head>

<body>
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div class="title">
                SISTEMA DE PROFORMAS
            </div>
            <div>
                <img class="logo-img" src="/assets/image/condor-mix.png" alt="">
            </div>

            <div class="content-btn-main" style="display: flex;">
                <a class="btn-main" href="<?php echo e(route('login')); ?>">PROPIETARIO DE TIENDA</a>
                <a class="btn-main" href="#">COTIZADOR</a>
            </div>
        </div>
    </div>
</body><?php /**PATH C:\laragon\www\condormix\resources\views/welcome.blade.php ENDPATH**/ ?>