@extends('layouts.app')

@section('content')

<div class="row justify-content-center">
    <div class="col-md-1 left-menu pad-main cont-icon-menu">
        <div class="icon-menu back-icon-menu">
            <a href="">
                <img src="/assets/image/home-video.png" alt="">
            </a>
        </div>
        <div class="icon-menu">
            <a href="">
                <img src="/assets/image/proforma.png" alt="">
            </a>
        </div>
        <div class="icon-menu">
            <a href="">
                <img src="/assets/image/download.png" alt="">
            </a>
        </div>
        <div class="icon-menu">
            <a href="">
                <img src="/assets/image/galeria.png" alt="">
            </a>
        </div>
    </div>
    <div class="col-md-1 pad-main content-panel-left">
        <div class="panel1">

        </div>
        <div class="panel2">

        </div>
    </div>
    <div class="col-md-9 pad-main">
        <video controls autoplay muted>
            <source src="/assets/videos/condor.mp4" type="video/mp4">
            Your browser does not support HTML5 video.
        </video>
    </div>
    <div class="col-md-1 pad-main">
        <div class="panel3">

        </div>
        <div class="panel4">

        </div>
        <div class="panel5">

        </div>
    </div>
</div>
@endsection